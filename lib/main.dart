import 'package:flutter/material.dart';
import 'package:flux_mobile/influxDB.dart';
import 'package:rapido/rapido.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'idping',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'idping'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DocumentList influxdbInstances = DocumentList(
    "account",
    labels: {
      "Friendly Name": "name",
      "Org Id": "org",
      "URL": "url",
      "Token": "token secret"
    },
    persistenceProvider: SecretsPercistence(),
  );

  DocumentList _results;

  @override
  void initState() {
    _results = DocumentList("rez", persistenceProvider: null);
    influxdbInstances.onLoadComplete = (DocumentList list) {
      _fetchData();
    };
    super.initState();
  }

  _fetchData() {
    influxdbInstances.forEach((Document instance) async {
      _results.clear();

      String queryString = "from(bucket: \"idping\") |> range(start: -5m)";

      InfluxDBAPI api = InfluxDBAPI(
        influxDBUrl: instance["url"],
        org: instance["org"],
        token: instance["token secret"],
      );

      InfluxDBTable table;
      InfluxDBQuery query = api.query(queryString);
      String error = "";
      try {
        List<InfluxDBTable> tables = await query.execute();
        table = tables[0];
      } catch (exception) {
        error = exception.toString();
      }

      Document result = Document(
        persistenceProvider: null,
        initialValues: {
          "error": error,
          "count": table != null ? table.rows.length : -1,
          "max": table == null ? 0 : table.rows.last["_value"],
          "name": instance["name"],
          "time": table == null || table.rows.length == 0
              ? "OFFLINE"
              : DateFormat("E").add_jm().format(
                    DateTime.parse(table.rows.last.utcString).toLocal(),
                  ),
        },
      );
      _results.add(result);
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return DocumentListScaffold(
      _results,
      title: "idping",
      customFAB: FloatingActionButton(
        onPressed: () {
          _fetchData();
        },
        child: Icon(Icons.refresh),
      ),
      additionalActions: [
        IconButton(
            icon: Icon(Icons.person),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (BuildContext context) {
                  return DocumentListScaffold(
                    influxdbInstances,
                    titleKeys: ["name"],
                    subtitleKey: "url",
                    emptyListWidget: Center(
                      child: Text("Use + to start adding accounts"),
                    ),
                  );
                }),
              );
            })
      ],

      customItemBuilder: (int index, Document doc, BuildContext context) {
        return InstanceReport(doc);
      }, // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class InstanceReport extends StatelessWidget {
  static Document row;

  InstanceReport(Document result) {
    row = result;
  }

  @override
  Widget build(BuildContext context) {
    return row["error"] == ""
        ? Card(
            margin: EdgeInsets.all(8.0),
            color: row["count"] >= 4
                ? Colors.green
                : row["count"] > 1
                    ? Colors.yellow
                    : Colors.red,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Center(
                    child: Text(
                      row["name"],
                      style: TextStyle(fontSize: 30.00),
                    ),
                  ),
                  Center(
                    child: Text(
                      row["time"].toString(),
                      style: TextStyle(fontSize: 20.00),
                    ),
                  ),
                  Center(
                    child: Text(
                      row["max"].toString(),
                    ),
                  ),
                ],
              ),
            ),
          )
        : Card(
            color: Colors.red,
            child: Column(
              children: [
                Center(
                  child: Text("ERROR"),
                ),
                Center(
                  child: Text(
                    row["error"],
                  ),
                ),
              ],
            ),
          );
  }
}
